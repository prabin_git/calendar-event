package com.example.prabin.calendar;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Prabin on 7/27/2018.
 */

public class ApplicationClass extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
